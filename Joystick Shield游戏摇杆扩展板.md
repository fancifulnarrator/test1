# Joystick Shield游戏摇杆扩展板



## 物理连接

### 传感器选择

   传感器选择如下图所示的型号为Joystick Shield的游戏摇杆扩展板模组。

![](https://gitee.com/fancifulnarrator/test1/raw/master/%E5%AE%9E%E7%89%A9%E5%9B%BE.jpg)

​		值的说明的是，Joystick Shield的游戏摇杆扩展板的电源有3.3V和5V两个选项和切换3.3V和5V的开关，但是WaFFle nano只能对外提供3.3V电压，因此在传感器本身没有提供开关的情况下，将切换3.3V/5V作为传感器的开/关。下文如果提到开关，一致见此处说明。



### 传感器接线

  传感器与Waffle Nano 之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连装态。

| Waffle Nano | 传感器 |
| ----------- | ------ |
| 3.3V        | 3.3V   |
| IO14        | A      |
| IO0         | B      |
| IO1         | C      |
| IO2         | D      |
| IO12        | X      |
| IO13        | Y      |
| GND         | GND    |

### 传感器接线示意图

![](https://gitee.com/fancifulnarrator/test1/raw/master/%E5%BC%95%E8%84%9A%E5%9B%BE%EF%BC%88%E5%AE%9E%E7%89%A9%EF%BC%89.jpg)



## 传感器库使用

  可以获取joystickshield.py（链接待补充）,将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`上。

### 库函数

```python
from machine import ADC, Pin
import utime


class JoystickShield:
    def __init__(self, pinA, pinB, pinC, pinD, adc1, adc2):
        # 从GPIO接口得到按键A、B、C、D的信号
        self.pinA = pinA
        self.pinB = pinB
        self.pinC = pinC
        self.pinD = pinD
        # 从ADC接口得到摇杆X、Y轴的信号
        self.adc1 = adc1
        self.adc2 = adc2
        utime.sleep(0.1)

    def read(self):
        A = self.pinA.value()
        B = self.pinB.value()
        C = self.pinC.value()
        D = self.pinD.value()
        self.adc1.equ(ADC.EQU_MODEL_8)
        self.adc1.atten(ADC.CUR_BAIS_DEFAULT)
        E = self.adc1.read()
        # 数据加工，将大于摇杆未动的稳定值的数值记为1（正方向移动），反之同理。摇杆稳定时，数值会在123,124浮动。
        if E > 124:
            E = 1
        elif E < 123:
            E = -1
        else:
            E = 0
        self.adc2.equ(ADC.EQU_MODEL_8)
        self.adc2.atten(ADC.CUR_BAIS_DEFAULT)
        F = self.adc2.read()
        if F > 124:
            F = 1
        elif F < 123:
            F = -1
        else:
            F = 0
        # 在返回的数组中，按钮：1未按下，0按下；摇杆：0不动，-1负方向，1正方向
        data = [A, B, C, D, E, F]
        return data
```
